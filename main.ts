// tell angular how to bootsrap the module and compile angular app in the browser
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule( AppModule );
