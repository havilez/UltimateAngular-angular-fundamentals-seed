import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { NotFoundComponent } from './not-found.component';
import { PassengerDashboardModule } from './passenger-dashboard/passenger-dashboard.module';
import { ComponentFactoryResolver } from '@angular/core/src/linker/component_factory_resolver';


const routes :Routes = [
  // home page route
  { path: '', redirectTo: 'passengers',  pathMatch: 'full'},
  // { path: '',  component: HomeComponent, pathMatch: 'full'},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  // register components with module here
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent
  ],
  imports: [
    // angular modules
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot(routes),
    // used to apply # (hash location) to url, supports hisotry , 'push state' in html5???
    //RouterModule.forRoot(routes, {useHash: true}),

    // custom modules
    PassengerDashboardModule
  ],
  bootstrap: [ AppComponent ] // tell module which component is top component
})

export class AppModule {

}
