// import angular component typescript decorator
import { Component } from '@angular/core';

// enables two way data binding
import { FormsModule } from '@angular/forms';

interface Nav {
  link :string,
  name :string,
  exact :boolean
};

// component decorates the defined class
// register component with angular
@Component({
  selector: 'app-root', //define html tag to load component into main html file
  styleUrls : ['app.component.scss'],
  templateUrl: './app.component.html'
})






export class AppComponent {
  nav :Nav[] = [
    {
      link: '/',
      name: 'Home',
      exact: true
    },
    {
      link: '/passengers',
      name: 'Passengers',
      exact: true
    },
    {
      link: '/oops',
      name: '404',
      exact: false
    },

  ];
}
