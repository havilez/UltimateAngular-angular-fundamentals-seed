import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import  'rxjs/add/operator/switchMap';

import { PassengerDashboardService } from '../../passenger-dashboard.service';
import { Passenger } from '../../models/passenger.interface';


@Component ({
  selector: 'passenger-viewer',
  styleUrls: ['passenger-viewer.component.scss'],
  templateUrl: 'passenger-viewer.component.html'
})

export class PassengerViewerComponent implements OnInit {
  passenger :Passenger;

  constructor(
    private router :Router,
    private route :ActivatedRoute,
    private passengerService :PassengerDashboardService) {};

  ngOnInit() {
    // ActivatedRoute params keeps track of url changes in id parameter
    // and an observable is returned on url parameter change
    this.route.params
      // subscribe to an event and returns an observable
      .switchMap((data :Passenger) => {
        // service takes parameter from switchMap and returns an observable
        return this.passengerService.getPassenger(data.id)
      })
      .subscribe((data :Passenger) => {
        this.passenger = data;
      })


  };

  onUpdatePassenger( event :Passenger) {
    console.log(event);
    // Note: updatePassenger call using toPromise conversion of Obsevable
    this.passengerService
      .updatePassenger(event) // make async call to persist data in event
      .then((data :Passenger) => { // callback?
        this.passenger = Object.assign({}, this.passenger, event);
      })
  }
  goBack() {
    // imperative routing
    this.router.navigate(['/passengers']);
  }
};


