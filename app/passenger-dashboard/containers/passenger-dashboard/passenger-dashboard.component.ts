import { Component, OnInit } from  '@angular/core';
import { Router } from '@angular/router';


import { Passenger } from '../../models/passenger.interface';
import { PassengerDashboardService } from  '../../passenger-dashboard.service';

@Component({
  selector: 'passenger-dashboard',
  styleUrls: ['passenger-dashboard.component.scss'],
  templateUrl: './passenger-dashboard.component.html'
})

export class PassengerDashboardComponent implements OnInit{
  passengers: Passenger[]= [];

  // dependency Inject PassengerDashboardService
  constructor(
    private router :Router,
    private passengerService :PassengerDashboardService) {
    // constructor signature is same as
    // this.router = Router;
    // this.passengerService = PassengerDashboardService;
  };

  ngOnInit() {
    console.log('ngOnInit');
     this.passengerService
      .getPassengers()
      .subscribe((data :Passenger[]) => {
        console.log('Data= ', data);
        this.passengers = data;
      })

    console.log('Number of passengers= ',this.passengers.length);
  }

  // custom event handlers to process output API of child component(s)
  handleEdit(event  :Passenger) {
    // return the immutable array of passengers with updated passenger changes
    this.passengerService
      .updatePassenger(event)
      .then((data)=> {
          this.passengers = this.passengers.map((passenger :Passenger) => {
            // check if passenger has been edited, then  update passenger properties
            if(passenger.id === event.id)
              passenger = Object.assign({},passenger,event);

            return passenger
        })
      })


  };

  handleRemove( event :Passenger ) {
    console.log('remove event data= ',event);
    // find selected passenger to remove in the event
    // then  remove from passenger list, using an immutable collection
    this.passengerService
    .removePassenger(event)
    .subscribe((data :Passenger) => {
      this.passengers = this.passengers.filter(( passenger :Passenger) => {
        return passenger.id !== event.id;
      });
    })


  };

  handleView( event :Passenger) {
    console.log('handleView called with parameter= ', event);
    // example of dynamic imperative routing
    this.router.navigate(['/passengers', event.id]);

  }

};
