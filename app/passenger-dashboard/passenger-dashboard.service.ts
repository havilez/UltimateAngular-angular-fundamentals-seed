import { Injectable } from '@angular/core';
import { Http, Response,Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Passenger } from '../passenger-dashboard/models/passenger.interface';


// api configured for json-server package
const PASSENGER_API :string = '/api/passengers';

// allows service to be injected into other classes
@Injectable()
export class PassengerDashboardService {
  constructor(private http :Http) {
    // same as this.http = Http;
   // console.log(this.http);
  }

  getPassengers() :Observable<Passenger[]> {
    return this.http
      .get(PASSENGER_API)
      .map((response :Response ) => {
        return response.json();
      })
  };

  getPassenger( id :number) :Observable<Passenger> {
    return this.http
      .get(`${PASSENGER_API}/${id}`)
      .map((response :Response ) => { // Note: imported map operator from rxjs
        return response.json();
      })
  };

  updatePassenger(passenger :Passenger) :Promise<Passenger> {
    // variables needed if you wish to create custom headers
    let headers = new Headers({
      'Content-Type' : 'application/json'
    });

    let options  = new RequestOptions({
      headers: headers
    })

    return this.http
      // .put(PASSENGER_API + '/' + passenger.id)
      .put(`${PASSENGER_API}/${passenger.id}`, passenger, options)
      .toPromise()
      .then((response :Response ) => {
          return response.json();
      });
  }

  removePassenger(passenger :Passenger) :Observable<Passenger> {
    return this.http
      // .put(PASSENGER_API + '/' + passenger.id)
      .delete(`${PASSENGER_API}/${passenger.id}`)
      .map((response :Response ) => {
        return response.json();
      })
  }


}
