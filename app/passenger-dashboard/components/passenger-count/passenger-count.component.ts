import { Component, Input } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';

@Component({
  selector: 'passenger-count',
  templateUrl: 'passenger-count.component.html'
})

export class PassengerCountComponent {
  // define input property binding
  @Input() items :Passenger[] = [];

  checkedInCount() :number {
    if (!this.items) return;
    return this.items.filter( (passenger :Passenger) => {
      return passenger.checkedIn; // creates new array if checkedIn flag is true on a given passenger
    }).length;
  }

  constructor() {};
}
