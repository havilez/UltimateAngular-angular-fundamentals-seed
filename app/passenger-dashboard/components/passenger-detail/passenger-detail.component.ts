import { Component, Input, Output, EventEmitter, OnChanges, OnInit } from '@angular/core';
import { Passenger } from '../../models/passenger.interface';

@Component({
  selector: 'passenger-detail',
  templateUrl: './passenger-detail.component.html',
  styleUrls: [ 'passenger-detail.component.scss']
})

export class PassengerDetailComponent implements OnChanges,OnInit {
  // data passed into this component from the parent component i.e. Input API
  @Input() detail :Passenger;

  // fire custom events to container component i.e Output API
  @Output()
  edit :EventEmitter<Passenger> = new EventEmitter<Passenger>();

  @Output()
  remove :EventEmitter<Passenger> = new EventEmitter<Passenger>();

  @Output()
  view :EventEmitter<Passenger> = new EventEmitter<Passenger>();

  editing :boolean=false;

  constructor() {};

  // angular life cycle hook events
  ngOnInit() {
    // console.log('ngOnInit');
  }

  // Note: ngOnChanges is called BEFORE ngOnInit
  ngOnChanges(changes) {
    // console.log(changes);
    // immutably change local data in child component
    if (changes.detail )  {
      this.detail  = Object.assign({},changes.detail.currentValue);
    }
    console.log('ngOnChanges');
  }

  // handler for input DOM event for input field in child component
  // https://developer.mozilla.org/en-US/docs/Web/Events/input
  onNameChange( value :string) {
    this.detail.fullname = value;
  };

  // handler for click DOM event handler for buttons in child component
  // https://developer.mozilla.org/en-US/docs/Web/Events/click
  // click event handler then fires custom event to parent component
  toggleEdit() {
    if ( this.editing ) {
      // this.eventName.emit(eventData)
      this.edit.emit(this.detail);
    }
      this.editing = !this.editing;  }


  onRemove() {
    // this.eventName.emit(eventData)
    this.remove.emit(this.detail);
  }

  goToPassenger() {
    console.log('goToPassenger called');

    // this.eventName.emit(eventData)
    this.view.emit( this.detail);
  }
}
