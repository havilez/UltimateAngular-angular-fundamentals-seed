import {NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Router, Routes } from '@angular/router';

// container
import { PassengerDashboardComponent } from './containers/passenger-dashboard/passenger-dashboard.component';
import { PassengerViewerComponent } from './containers/passenger-viewer/passenger-viewer.component';

// components
import  { PassengerCountComponent } from './components/passenger-count/passenger-count.component';
import  { PassengerDetailComponent } from './components/passenger-detail/passenger-detail.component';
import  { PassengerFormComponent }   from '../passenger-dashboard/components/passenger-form/passenger-form.component';

// service
import { PassengerDashboardService } from './passenger-dashboard.service';
import { RouterModule } from '@angular/router';

const routes :Routes = [
  {
    path: 'passengers',
    children: [
      { path: '', component: PassengerDashboardComponent},
      { path: ':id' , component: PassengerViewerComponent}
    ],
  }
]

@NgModule({
  // holds all components used in this feature module
  declarations: [
    // containers
    PassengerDashboardComponent,
    PassengerViewerComponent,
    // components
    PassengerCountComponent,
    PassengerDetailComponent,
    PassengerFormComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  // NOT NEEDED once routing is implemented
  // exports: [
  //   // allows exported components to be used in the application
  //   PassengerDashboardComponent,
  //   PassengerViewerComponent
  // ],
  providers: [
    PassengerDashboardService
  ]
})

export class PassengerDashboardModule {

}
